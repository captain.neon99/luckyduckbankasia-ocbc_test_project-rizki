/**
 * @format
 */

import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import * as redux from 'react-redux';
import SplashScreen from './../src/screens/SplashScreen';
import AuthLogin from './../src/screens/AuthLogin';
import AuthRegister from './../src/screens/AuthRegister';
import Dashboard from './../src/screens/Dashboard';
import {NavigationContext} from '@react-navigation/native';
import NewTransaction from './../src/screens/NewTransaction';
import TransactionSummary from '../src/screens/TransactionSummary';
import Accounts from './../src/screens/Accounts';

jest.useFakeTimers();

//Mock Navigation And Route Props from React Navigation
const navigationProps = {
  replace: () => {},
  navigate: () => {},
  isFocused: () => true,
  addListener: jest.fn(() => jest.fn()),
};

const routeProps = {
  params: {
    fromNewTransaction: false,
    transactionDetails: {
      transactionType: 'received',
      sender: {accountNo: '0000-000-0000', accountHolder: 'Tester'},
      transactionDate: 1647148607598,
    },
  },
};

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(() => jest.fn()),
}));

//Mock Store State
const state = {
  payees: {
    payeesList: [
      {name: 'Andy', accountNo: '6554-630-9653'},
      {name: 'Lina', accountNo: '6554-630-9654'},
    ],
  },
  transaction: {
    success: true,
    transactionLastAttemptDate: 1647148607598,
  },
  banking: {
    userBalance: 1000,
    userTransaction: [
      {
        amount: 11,
        description: null,
        receipient: {
          accountHolder: 'Andy',
          accountNo: '6554-630-9653',
        },
        transactionDate: '2022-03-12T14:08:14.681Z',
        transactionId: '622ca94ea2426e3f3c4a1752',
        transactionType: 'transfer',
      },
      {
        amount: 11,
        description: null,
        sender: {
          accountHolder: 'Andy',
          accountNo: '6554-630-9653',
        },
        transactionDate: '2022-03-12T14:08:14.681Z',
        transactionId: '622ca94ea2426e3f3c4a1752',
        transactionType: 'received',
      },
    ],
  },
  user: {
    code: 200,
    loading: false,
    message: 'Login Success',
    stage: 2,
    success: true,
    userData: {userAccountNumber: '0884-561-1550', userName: 'mashipro'},
    userLatestChangeTime: 1647149740881,
    userLoginTime: 1647149740881,
    userToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjJi',
  },
  global: {
    userGlobalState: [
      {
        userAccountNumber: '0000-000-0000',
        userCardData: {
          cardColorScheme: 0,
        },
        stateLastChange: null,
      },
      {
        stateLastChange: 1647089411274,
        userAccountNumber: '0884-561-1550',
        userCardData: {cardColorScheme: 1},
        stateLastChange: 1647149740881,
      },
    ],
  },
};

jest
  .spyOn(redux, 'useSelector')
  .mockImplementation(callback => callback(state));

describe('RenderTest SplashScreen', () => {
  const screen = renderer
    .create(<SplashScreen navigation={navigationProps} />)
    .toJSON();

  it('should Render Correctly', () => {
    expect(screen).toMatchSnapshot();
  });

  it('should Navigate to MainScreen', () => {
    spyOn(navigationProps, 'replace');
    jest.advanceTimersByTime(2000);
    expect(navigationProps.replace).toHaveBeenCalledWith('MainScreen');
  });
});

describe('RenderTest LoginScreen', () => {
  const screen = renderer
    .create(<AuthLogin navigation={navigationProps} route={routeProps} />)
    .toJSON();

  it('should Render Correctly', () => {
    expect(screen).toMatchSnapshot();
  });
});

describe('RenderTest AuthRegister', () => {
  const screen = renderer
    .create(<AuthRegister navigation={navigationProps} route={routeProps} />)
    .toJSON();

  it('should Render Correctly', () => {
    redux.useDispatch.mockReturnValue(jest.fn());
    expect(screen).toMatchSnapshot();
  });
});

describe('RenderTest Dashboard', () => {
  const screen = renderer
    .create(
      <NavigationContext.Provider value={navigationProps}>
        <Dashboard navigation={navigationProps} route={routeProps} />
      </NavigationContext.Provider>,
    )
    .toJSON();
  it('should Render Correctly', () => {
    expect(screen).toMatchSnapshot();
  });
});

describe('RenderTest NewTransaction', () => {
  const screen = renderer
    .create(
      <NavigationContext.Provider value={navigationProps}>
        <NewTransaction navigation={navigationProps} route={routeProps} />
      </NavigationContext.Provider>,
    )
    .toJSON();

  it('should Render Correctly', () => {
    expect(screen).toMatchSnapshot();
  });
});

describe('RenderTest TransactionSummary', () => {
  const screen = renderer
    .create(
      <NavigationContext.Provider value={navigationProps}>
        <TransactionSummary navigation={navigationProps} route={routeProps} />
      </NavigationContext.Provider>,
    )
    .toJSON();

  it('should Render Correctly', () => {
    expect(screen).toMatchSnapshot();
  });
});

describe('RenderTest Accounts', () => {
  const screen = renderer
    .create(
      <NavigationContext.Provider value={navigationProps}>
        <Accounts navigation={navigationProps} route={routeProps} />
      </NavigationContext.Provider>,
    )
    .toJSON();

  it('should Render Correctly', () => {
    expect(screen).toMatchSnapshot();
  });
});
