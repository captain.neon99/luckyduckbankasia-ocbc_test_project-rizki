/**
 * @format
 */

import 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import Button from '../src/components/Button';
import CardBaseTransaction from './../src/components/CardBaseTransaction';
import CardTransaction from './../src/components/CardTransaction';
import ColorButton from '../src/components/ColorButton';
import Separator from '../src/components/Separator';
import ToastLayout from '../src/components/ToastLayout';
import UserBalance from '../src/components/UserBalance';
import UserCard from './../src/components/UserCard';
import InputText from './../src/components/InputText';

jest.useFakeTimers();

describe('All Components Must Be Passed', () => {
  it('should Render Button Correctly', () => {
    const components = renderer.create(<Button />).toJSON();
    expect(components).toMatchSnapshot();
  });

  it('should Render CardBaseTransaction Correctly', () => {
    const cardData = {transactionDate: '2022-03-12T14:08:14.681Z'};
    const components = renderer
      .create(<CardBaseTransaction cardData={cardData} />)
      .toJSON();
    expect(components).toMatchSnapshot();
  });

  it('should Render CardTransaction Correctly', () => {
    const cardData = {
      amount: 11,
      description: null,
      receipient: {accountNo: '6554-630-9653', accountHolder: 'Andy'},
      transactionDate: '2022-03-12T14:08:14.681Z',
      transactionId: '622ca94ea2426e3f3c4a1752',
      transactionType: 'transfer',
    };
    const components = renderer
      .create(<CardTransaction cardData={cardData} />)
      .toJSON();
    expect(components).toMatchSnapshot();
  });

  it('should Render ColorButton Correctly', () => {
    const components = renderer.create(<ColorButton />).toJSON();
    expect(components).toMatchSnapshot();
  });

  it('should Render Separator Correctly', () => {
    const components = renderer.create(<Separator />).toJSON();
    expect(components).toMatchSnapshot();
  });

  it('should Render ToastLayout Correctly', () => {
    const props = {
      onPress: () => {},
      hide: () => {},
    };
    const components = renderer.create(<ToastLayout props={props} />).toJSON();
    expect(components).toMatchSnapshot();
  });

  it('should Render UserBalance Correctly', () => {
    const components = renderer.create(<UserBalance />).toJSON();
    expect(components).toMatchSnapshot();
  });

  it('should Render UserCard Correctly', () => {
    const props = {
      cardColorScheme: 0,
    };
    const user = {
      userAccountNumber: '0000-000-0000',
    };
    const components = renderer
      .create(<UserCard cardData={props} user={user} />)
      .toJSON();
    expect(components).toMatchSnapshot();
  });

  it('should Render InputText Correctly', () => {
    const props = {label: 'Test Label'};
    const components = renderer.create(<InputText {...props} />).toJSON();
    expect(components).toMatchSnapshot();
  });
});
