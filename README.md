# LuckyDuckBankAsia-OCBC_test_project-Rizki

## Lucky Duck Bank Asia

This Apps is a test apps, built for my OCBC Enrolment test.

## Required Features

- [x] Login User
- [x] Register new User (optional)
- [x] Check User Balance
- [x] Check User Payees (optional)
- [x] Check User Transaction Histories
- [x] Make new Transaction (optional)

## Apps Tech Stack

The apps has few dependencies used

- Redux
- Redux Thunk
- Redux Persist
- React Navigation
- Axios
- React Native Toast Message

Redux Thunk is used due to its simplicity and faster debugging time and Redux Persist used along with AsyncStorage to persist user base data, token, and user preferred Card Color.
The Card color must be correspond to the apps user and reset when the apps is uninstalled.

## Screens

There are some Screen used in this project to ensure the Apps good working capabilities as intended.
This in part add some degree of complexities in the code but keeping it simple in apps view is always main priority.

Apps Screen:

- Splash Screen
- Auth Login
- Auth Register
- Dashboard
- New Transaction
- Transaction Summary
- Accounts
- Devscreen (Used only for Development)

All of the screens above except Splash Screen is using user authentication as dependencies.
While the user is unauthenticated, user can only access Auth Login and Auth Register screen and in turn in authenticated state, user cannot access Auth Login and Auth Register. First screen shown in authenticated state is Dashboard where it shows user balance and transaction history.

All of user transaction history can be pressed to move screen into its transaction details.
This transaction details is reusable in which used to show user about details of each transaction history and transaction status after user make a new transaction.

Devscreen is available but not in used in user screen. This screen only used for visual test and debug the components

## Components

Some of the apps components are used instead of native components to eliminate the needs to use external UI-Libraries.
All components are reusable and been custom made to be versatile in its implementation.
This versatility come with some complexities due to some base components has the needs to be multi-state or different modes.

## Global Values

Global values been used to store some value which may be used globally across the application.

Global Values:

- Global Values
- Global URL
- Global Color
- Global Style
- Global Action Type

Some values are used to change UI and some others to compliment application function such as Action and HTTP Request.

These value provide one-stop-service to change application parameter based on the needed parameter

## Utilities and Manager

Most of this Apps functional function is executed in Redux Action. To access or execute HTTP Request, all action should execute other function from manager. This is done to simplify the redux action and ignore all HTTP Request aspect of it except its returned value.

Manager only dealing with Axios HTTP Request and nothing should be done here.

Manager:

- Auth Manager - this manager used for authentication
- Banking Manager - this manager used for apps user functionality

This apps also separate most of reusable function as utilities. Due to apps simplicity. All utilities function is written in one file. This utilities only dealing with some data manipulation and mutation such as Transaction array sorting by inbound/outbound transaction or by transaction date.

## Testing

All testing covered:

- [x] Component Render Testing
- [x] Screen Render Testing
