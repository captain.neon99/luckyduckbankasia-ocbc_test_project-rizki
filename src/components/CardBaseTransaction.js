import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {GlobalColor} from '../configs/GlobalValue';
import {GlobalStyle} from './../configs/GlobalValue';

const CardBaseTransaction = ({cardData, children}) => {
  return (
    <View
      style={{
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 5,
        elevation: 5,
        margin: 10,
      }}>
      <Text
        style={[
          GlobalStyle.Content,
          {color: GlobalColor.grey, fontWeight: '600'},
        ]}>
        {cardData.transactionDate}
      </Text>
      {children}
    </View>
  );
};

export default CardBaseTransaction;

const styles = StyleSheet.create({});
