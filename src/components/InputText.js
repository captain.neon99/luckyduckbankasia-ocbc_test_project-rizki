import {TouchableOpacity, Text, View, TextInput, Animated} from 'react-native';
import React, {useEffect, useRef, useState, forwardRef} from 'react';
import {GlobalColor} from './../configs/GlobalValue';
// import {} from 'react-native-gesture-handler';
import {getCurrencyString} from './../functions/Utilities';

const InputText = forwardRef(
  (
    {
      label,
      value = '',
      onChangeText = () =>
        !dropdown ? console.log('please override onChangeText') : {},
      onSubmitEditing = () => {},
      onFocus = () => {},
      style,
      inputStyle,
      labelStyle,
      secured = false,
      disableAnimation = false,
      disabled = false,
      error = false,
      animationDuration,
      keyboardType,
      multiline = false,
      numberOfLines,
      dropdown = false,
      dropdownData = [],
      dropdownLoading = false,
      onDropdownChange = () => console.log('please override onDropdownChange'),
      dropdownContainerStyle,
      dropdownItemStyle,
      dropdownItemSelectedStyle,
      dropdownItemTextStyle,
      dropdownItemSelectedTextStyle,
      dropdownIndex = 2,
      suffix,
      suffixStyle,
      onSuffixPressed = () => console.log('please override onSuffixPressed'),
      isCurrency = false,
    },
    ref,
  ) => {
    const [height, setHeight] = useState(0);
    const [componentHeight, setComponentHeight] = useState(0);
    const [componentWidth, setComponentWidth] = useState(0);
    const [focused, setFocused] = useState(false);
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [selectedDropdown, setSelectedDropdown] = useState();
    const [focusedOnce, setFocusedOnce] = useState(false);

    //Animation Reference
    const positionY = useRef(new Animated.Value(0)).current;
    const colorAnimationRef = useRef(new Animated.Value(1)).current;

    //Color Animation Interpolator
    const interpColor = colorAnimationRef.interpolate({
      inputRange: [0, 1, 2],
      outputRange: [
        GlobalColor.primary,
        GlobalColor.lightest,
        GlobalColor.error,
      ],
    });

    //Animation Duration default
    const duration = animationDuration ?? 250;

    useEffect(() => {
      if (focused && !focusedOnce) {
        setFocusedOnce(true);
      }
      if (disableAnimation) return;
      if (!focused) {
        //Start on blur color Animation
        Animated.timing(colorAnimationRef, {
          toValue: 1,
          duration: duration,
          delay: 0,
          useNativeDriver: false,
        }).start();
        return;
      }
      //   console.log('focused');

      //Start on focus position Animation
      Animated.parallel([
        Animated.timing(positionY, {
          toValue: 1,
          duration: duration,
          delay: 0,
          useNativeDriver: true,
        }),
        Animated.timing(colorAnimationRef, {
          toValue: 0,
          duration: duration,
          delay: 0,
          useNativeDriver: false,
        }),
      ]).start();

      onFocus();
    }, [focused]);

    useEffect(() => {
      if (disableAnimation) return;
      if (!error) return;
      //Start on error color Animation
      Animated.timing(colorAnimationRef, {
        toValue: 2,
        duration: duration,
        delay: 0,
        useNativeDriver: false,
      }).start();
    }, [error]);

    useEffect(() => {
      if (value.length >= 1) {
        //Set position default when value is present
        Animated.timing(positionY, {
          toValue: 1,
          duration: duration,
          delay: 0,
          useNativeDriver: true,
        }).start();
      }
    }, []);

    useEffect(() => {
      if (!dropdown) return;
      if (selectedDropdown == null) return;
      onChangeText(selectedDropdown.label);
    }, [selectedDropdown]);

    return (
      <Animated.View
        onLayout={event => {
          setComponentHeight(event.nativeEvent.layout.height);
          setComponentWidth(event.nativeEvent.layout.width);
        }}
        style={[
          {
            borderWidth: 1,
            borderRadius: 5,
            borderColor: interpColor,
            paddingHorizontal: 5,
            backgroundColor: GlobalColor.background,
            marginTop: disableAnimation ? 0 : componentWidth / 25,
            zIndex: 1 + dropdownIndex,
          },
          style,
        ]}>
        {label && (
          <Animated.Text
            onLayout={event => {
              setHeight(event.nativeEvent.layout.height);
            }}
            style={[
              {
                position: disableAnimation ? 'relative' : 'absolute',
                left: 5,
                transform: [
                  {
                    translateY: disableAnimation
                      ? 0
                      : positionY.interpolate({
                          inputRange: [0, 1],
                          outputRange: [
                            componentHeight / 2 - height / 1.5,
                            -height / 1.5,
                          ],
                        }),
                  },
                ],
              },
              {
                justifyContent: 'center',
                color: GlobalColor.lightest,
                backgroundColor: GlobalColor.background,
                textAlignVertical: 'center',
                paddingHorizontal: 5,
                marginVertical: 2,
              },
              labelStyle,
            ]}>
            {label}
          </Animated.Text>
        )}
        {dropdown && dropdownOpen && (
          <View
            style={[
              {
                position: 'absolute',
                top: componentHeight - 1,
                left: -1,
                backgroundColor: GlobalColor.background,
                padding: 5,
                borderBottomRightRadius: 5,
                borderBottomLeftRadius: 5,
                elevation: 5,
                zIndex: dropdownIndex,
              },
              dropdownContainerStyle,
            ]}>
            {dropdownLoading ? (
              <Text
                style={[
                  {
                    height: componentHeight - 10,
                    width: componentWidth - 10,
                    textAlignVertical: 'center',
                  },
                  dropdownItemTextStyle,
                ]}>
                Loading Data ...{' '}
              </Text>
            ) : (
              dropdownData.map((item, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    style={[
                      {
                        height: componentHeight - 10,
                        width: componentWidth - 10,
                        paddingHorizontal: 5,
                        justifyContent: 'center',
                      },
                      value == item.label ||
                      value == item.value ||
                      selectedDropdown?.value == item.value
                        ? [
                            {
                              borderRadius: 5,
                              backgroundColor: GlobalColor.lightest,
                              elevation: 5,
                              paddingHorizontal: 5,
                            },
                            dropdownItemSelectedStyle,
                          ]
                        : {},
                      dropdownItemStyle,
                    ]}
                    onPress={() => {
                      setDropdownOpen(!dropdownOpen);
                      setSelectedDropdown(item);
                      onDropdownChange(item.value);
                    }}>
                    <Text
                      style={[
                        selectedDropdown?.value == item.value
                          ? [
                              {color: GlobalColor.background},
                              dropdownItemSelectedTextStyle,
                            ]
                          : {color: GlobalColor.lightest},
                        dropdownItemTextStyle,
                      ]}>
                      {item.label}
                    </Text>
                  </TouchableOpacity>
                );
              })
            )}
          </View>
        )}
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          {isCurrency && focusedOnce && (
            <Text style={{color: GlobalColor.lightest}}>SGD</Text>
          )}
          <TextInput
            style={[
              {flex: 1, color: GlobalColor.lightest},
              multiline
                ? {textAlignVertical: 'top', maxHeight: componentWidth / 2}
                : {},
              inputStyle,
            ]}
            value={dropdown ? selectedDropdown?.label : value}
            onChangeText={t => onChangeText(t)}
            blurOnSubmit={!multiline}
            editable={!disabled}
            secureTextEntry={secured ?? false}
            multiline={multiline}
            numberOfLines={numberOfLines ?? null}
            maxLength={120}
            keyboardType={isCurrency ? 'number-pad' : keyboardType ?? 'default'}
            onFocus={() => {
              setFocused(true);
              if (dropdown) {
                setDropdownOpen(true);
              }
            }}
            onBlur={() => {
              setFocused(false);
              if (dropdown) {
                setDropdownOpen(false);
                return;
              }
            }}
            ref={ref}
            onSubmitEditing={() => {
              if (onSubmitEditing) onSubmitEditing();
            }}
          />
          {isCurrency && focusedOnce && (
            <Text style={{color: GlobalColor.lightest}}>.00</Text>
          )}
          <TouchableOpacity
            style={{marginHorizontal: 5}}
            onPress={() => {
              console.log('pressed');
              if (dropdown) {
                setDropdownOpen(!dropdownOpen);
                setFocused(!focused);
                return;
              }
              onSuffixPressed();
            }}>
            <Text
              style={[
                {textAlignVertical: 'center', color: GlobalColor.lightest},
                suffixStyle,
              ]}>
              {dropdown ? (!dropdownOpen ? 'choose' : 'close') : suffix ?? ''}
            </Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    );
  },
);

export default InputText;
