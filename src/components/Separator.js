import {View} from 'react-native';
import React from 'react';

const Separator = ({style, height = 0, width = 0}) => {
  return <View style={[{height, width}, style]} />;
};

export default Separator;
