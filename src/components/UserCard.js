import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import React, {useState} from 'react';
import Separator from './Separator';
import {getCardColor, getCurrencyString} from '../functions/Utilities';
import {GlobalStyle, GlobalColor} from './../configs/GlobalValue';
import {
  capitalizeSentence,
  obscureAccountNumber,
} from './../functions/Utilities';

const UserCard = ({user, balance = 0, cardData}) => {
  const {width, height} = Dimensions.get('screen');

  const [obscured, setObscured] = useState(true);

  return (
    <View
      style={[
        styles.Base,
        {
          height: width / 2,
          backgroundColor: getCardColor(cardData.cardColorScheme),
          justifyContent: 'space-between',
          overflow: 'hidden',
        },
      ]}>
      <ImageBackground
        source={require('../assets/Taieri.png')}
        imageStyle={{
          left: width / 2,
          top: 0,
          height: '150%',
          width: '150%',
          resizeMode: 'cover',
        }}
        style={[
          // StyleSheet.absoluteFill,
          {
            backgroundColor: getCardColor(cardData.cardColorScheme),
            justifyContent: 'space-between',
            padding: 15,
            flex: 1,
            // left: 20,
          },
        ]}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={[GlobalStyle.Hero, {textAlign: 'right'}]}>LDBA</Text>
            <Text style={[GlobalStyle.SubHero, {textAlign: 'right'}]}>
              Lucky Duck Bank Asia
            </Text>
          </View>
        </View>
        <View>
          <View style={{flexDirection: 'row'}}>
            <Text style={[GlobalStyle.Balance, {fontWeight: 'bold'}]}>
              {obscured
                ? obscureAccountNumber(user.userAccountNumber)
                : user.userAccountNumber}
            </Text>
            <Separator width={10} />
            <TouchableOpacity
              style={{justifyContent: 'center'}}
              onPress={() => setObscured(!obscured)}>
              <Text style={[GlobalStyle.Content]}>
                {obscured ? 'show' : 'hide'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <Text style={GlobalStyle.Title}>
          {capitalizeSentence(user.userName)}
        </Text>
      </ImageBackground>
    </View>
  );
};

export default UserCard;

const styles = StyleSheet.create({
  Base: {
    borderRadius: 5,
    elevation: 5,
    margin: 15,
  },
});
