import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {GlobalColor} from '../configs/GlobalValue';

const ToastLayout = ({
  props,
  success = false,
  warning = false,
  notice = false,
  // onPress,
}) => {
  const {width, height} = Dimensions.get('screen');
  // console.log(props);
  return (
    <TouchableOpacity
      // disabled={onPress == null}
      onPress={props.onPress()}
      style={{
        padding: 15,
        backgroundColor: success
          ? GlobalColor.primary
          : warning
          ? GlobalColor.tertiary
          : notice
          ? GlobalColor.lightest
          : GlobalColor.error,
        elevation: 3,
        width: width - 30,
        // marginHorizontal: 15,
      }}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <Text
          numberOfLines={3}
          style={[
            // GlobalStyle.style.textBody2,
            {
              color:
                warning || notice ? GlobalColor.primary : GlobalColor.lightest,
              flex: 1,
            },
          ]}>
          {props.text1}
        </Text>
        <View style={{width: 10}} />
        <TouchableOpacity onPress={() => props.hide()}>
          <Text
            numberOfLines={3}
            style={[
              // GlobalStyle.style.textBody2,
              {
                color:
                  warning || notice ? GlobalColor.primary : GlobalColor.white,
                borderColor:
                  warning || notice ? GlobalColor.primary : GlobalColor.white,
                borderBottomWidth: 1,
                // flex: 1,
              },
            ]}>
            hide
          </Text>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

export default ToastLayout;

const styles = StyleSheet.create({});
