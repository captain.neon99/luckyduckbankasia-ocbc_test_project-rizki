import {
  ImageBackground,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import React from 'react';
import {GlobalColor, GlobalStyle} from './../configs/GlobalValue';
import {getCurrencyString} from './../functions/Utilities';

const UserBalance = ({userBalance, disable = false, onPress = () => {}}) => {
  const {width, height} = Dimensions.get('screen');
  return (
    <TouchableOpacity
      disabled={disable}
      onPress={onPress}
      style={{
        margin: 15,
        // padding: 10,
        borderRadius: 5,
        backgroundColor: GlobalColor.primary,
        overflow: 'hidden',
      }}>
      <ImageBackground
        source={require('../assets/Taieri.png')}
        imageStyle={{
          left: width / 2,
          top: 0,
          height: '150%',
          width: '150%',
          resizeMode: 'cover',
        }}
        style={[
          // StyleSheet.absoluteFill,
          {
            justifyContent: 'space-between',
            padding: 15,
            // flex: 1,
            // left: 20,
          },
        ]}>
        <Text style={[GlobalStyle.Content, {fontWeight: 'bold'}]}>
          My Balance
        </Text>
        <Text style={[GlobalStyle.Hero, {}]}>
          {getCurrencyString(userBalance)}
        </Text>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default UserBalance;
