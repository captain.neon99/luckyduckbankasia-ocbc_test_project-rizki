import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {GlobalColor} from './../configs/GlobalValue';

export default function Button({
  disable = false,
  mode = 'fill' || 'bordered' || 'text',
  style,
  textStyle,
  onPress = () => {},
  label,
}) {
  const defaultMode = mode ?? 'fill';
  return (
    <TouchableOpacity
      disabled={disable ?? false}
      style={[
        {
          paddingHorizontal: 15,
          paddingVertical: 10,
          borderRadius: 5,
          borderWidth: defaultMode == 'bordered' ? 1 : 0,
          borderColor: GlobalColor.primary,
        },
        defaultMode == 'fill' ? {backgroundColor: GlobalColor.primary} : null,
        style,
      ]}
      onPress={() => onPress()}>
      <Text
        style={[
          {
            fontWeight: 'bold',
            fontSize: 16,
            color:
              defaultMode == 'fill'
                ? GlobalColor.lightest
                : GlobalColor.primary,
            textAlign: 'center',
          },
          textStyle,
        ]}>
        {label}
      </Text>
    </TouchableOpacity>
  );
}
