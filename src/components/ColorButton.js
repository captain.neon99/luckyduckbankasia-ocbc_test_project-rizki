import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {GlobalColor} from './../configs/GlobalValue';

const ColorButton = ({
  size = 35,
  color,
  active = false,
  onPress = () => {},
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        height: active ? size : size,
        width: active ? size : size,
        backgroundColor: color ?? GlobalColor.primary,
        borderRadius: size,
        borderWidth: active ? 3 : 0,
        borderColor: GlobalColor.lightest,
      }}
    />
  );
};

export default ColorButton;
