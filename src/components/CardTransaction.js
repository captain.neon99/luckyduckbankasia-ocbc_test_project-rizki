import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import {getCurrencyString, capitalizeSentence} from './../functions/Utilities';
import {GlobalStyle, GlobalColor} from './../configs/GlobalValue';
import moment from 'moment';

const CardTransaction = ({
  cardData,
  attachDate = false,
  onPress = () => {},
}) => {
  const isIncoming = cardData.transactionType == 'received';
  const toOrFrom = isIncoming ? cardData.sender : cardData.receipient;
  const amount = cardData.amount;
  return (
    <TouchableOpacity style={{padding: 5}} onPress={onPress}>
      {attachDate && (
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={{color: GlobalColor.grey}}>
            {moment(cardData.transactionDate).format('DD MMM YYYY || hh:mm')}
          </Text>
          <Text style={{color: GlobalColor.darkest}}>
            {moment(cardData.transactionDate).fromNow()}
          </Text>
        </View>
      )}
      <View style={{flexDirection: 'row'}}>
        <View style={{flex: 1}}>
          <Text
            style={[
              {color: GlobalColor.darkest, fontWeight: 'bold', fontSize: 18},
            ]}>
            {toOrFrom.accountHolder}
          </Text>
          <Text style={[{color: GlobalColor.dark}]}>{toOrFrom.accountNo}</Text>
        </View>
        <View style={{justifyContent: 'center'}}>
          <Text style={{textAlign: 'right', color: GlobalColor.grey}}>
            {capitalizeSentence(cardData.transactionType)}
          </Text>
          <Text
            style={[
              {
                color: isIncoming ? GlobalColor.success : GlobalColor.error,
                fontWeight: 'bold',
                fontSize: 18,
              },
            ]}>
            {isIncoming ? '+ ' : '- '}
            {getCurrencyString(amount)}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CardTransaction;

const styles = StyleSheet.create({});
