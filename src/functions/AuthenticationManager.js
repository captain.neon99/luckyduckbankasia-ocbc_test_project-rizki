import axios from 'axios';
import {GlobalURL} from './../configs/GlobalValue';

const AuthenticationManager = {
  authLogin: async (username, password) => {
    return await axios({
      url: GlobalURL('login').url,
      method: GlobalURL('login').method,
      data: {username, password},
    });
  },
  authRegister: async (username, password) => {
    return await axios({
      url: GlobalURL('register').url,
      method: GlobalURL('register').method,
      data: {username, password},
    });
  },
};

export default AuthenticationManager;
