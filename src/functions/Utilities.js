import moment from 'moment';
import {GlobalColor} from '../configs/GlobalValue';

export const getCurrencyString = (balance = 0) => {
  return `SGD ${balance.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}`;
};

export const transactionSorter = (baseTransactionData, mode) => {
  if (baseTransactionData?.length < 1 || baseTransactionData == null) return [];

  const sortByDate = parameter => {
    var sortedTransaction = [];
    baseTransactionData.forEach(element => {
      const elementTransactionDate = moment(element.transactionDate).format(
        `${parameter == 'date' ? 'DD MMMM YYYY' : 'MMMM YYYY'}`,
      );
      const existedDateIndex = sortedTransaction.findIndex(
        item => item.transactionDate == elementTransactionDate,
      );
      if (existedDateIndex < 0) {
        sortedTransaction.push({
          transactionDate: elementTransactionDate,
          transactions: [element],
        });
      } else {
        sortedTransaction[existedDateIndex].transactions.push(element);
      }
    });
    // console.log('sortedTransaction', sortedTransaction);
    return sortedTransaction;
  };

  const sortByPeople = parameter => {
    var sortedTransaction = [];
    baseTransactionData.forEach(element => {
      const isReceived = element.transactionType == 'received';
      const elementTransactionAccountNo = isReceived
        ? element.sender.accountNo
        : element.receipient.accountNo;
      const elementTransactionAccountHolder = isReceived
        ? element.sender.accountHolder
        : element.receipient.accountHolder;
      const existedAccountIndex = sortedTransaction.findIndex(
        item => item.accountNo == elementTransactionAccountNo,
      );
      if (existedAccountIndex < 0) {
        sortedTransaction.push({
          accountNo: elementTransactionAccountNo,
          accountHolder: elementTransactionAccountHolder,
          transactions: [element],
        });
      } else {
        sortedTransaction[existedAccountIndex].transactions.push(element);
      }
    });
    // console.log('sortedTransaction', sortedTransaction);
    return sortedTransaction;
  };

  const sortByInOut = () => {
    var sortedTransaction = [];
    baseTransactionData.forEach(element => {
      const existedAccountIndex = sortedTransaction.findIndex(
        item => item.transactionType == element.transactionType,
      );
      if (existedAccountIndex < 0) {
        sortedTransaction.push({
          transactionType: element.transactionType,
          transactions: [element],
        });
      } else {
        sortedTransaction[existedAccountIndex].transactions.push(element);
      }
    });
    // console.log('sortedTransaction', sortedTransaction);
    return sortedTransaction;
  };

  switch (mode) {
    case 'date':
      return sortByDate('date');

    case 'month':
      return sortByDate('month');

    case 'people':
      return sortByPeople();

    case 'inOut':
      return sortByInOut();

    default:
      return sortByDate('date');
  }
};

export const payeesDropdownTransformer = payeesData => {
  if (payeesData == null || payeesData.length < 1) return [];
  var transformedPayees = [];
  payeesData.forEach(element => {
    transformedPayees.push({label: element.name, value: element.accountNo});
  });
  return transformedPayees;
};

export class TransferDataset {
  constructor(receipientAccountNo, amount, description) {
    this.receipientAccountNo = receipientAccountNo;
    this.amount = amount;
    this.description = description;
  }
  toJson = () => {
    return {
      receipientAccountNo: this.receipientAccountNo,
      amount: this.amount,
      description: this.description,
    };
  };
}

export const capitalizeSentence = paragraph => {
  if (paragraph == null || paragraph.length < 1) return;
  const strings = paragraph
    .toLowerCase()
    .split('. ')
    .map(sentence => sentence.charAt(0).toUpperCase() + sentence.substring(1))
    .join('. ')
    .split('\n')
    .map(sentence => sentence.charAt(0).toUpperCase() + sentence.substring(1))
    .join('\n');
  return strings;
};

export const getCardColor = cardColorScheme => {
  switch (cardColorScheme) {
    case 0:
      return GlobalColor.primary;
    case 1:
      return GlobalColor.secondary;
    case 2:
      return GlobalColor.tertiary;
    default:
      break;
  }
};

export const obscureAccountNumber = number => {
  if (number == null) return '0000-000-0000';
  const strings = number.split('-');
  return `XXXX-XXX-${strings[2]}`;
};
