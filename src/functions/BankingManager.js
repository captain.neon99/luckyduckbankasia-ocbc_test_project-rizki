import axios from 'axios';
import {GlobalURL} from '../configs/GlobalValue';

const BankingManager = {
  getBalance: async token => {
    console.log('getting balance');
    return await axios({
      url: GlobalURL('balance').url,
      method: GlobalURL('balance').method,
      headers: {
        Authorization: token,
      },
    });
  },
  getTransaction: async token => {
    console.log('getting transaction');
    return await axios({
      url: GlobalURL('transaction').url,
      method: GlobalURL('transaction').method,
      headers: {
        Authorization: token,
      },
    });
  },
  getPayees: async token => {
    console.log('getting payees');
    return await axios({
      url: GlobalURL('payees').url,
      method: GlobalURL('payees').method,
      headers: {
        Authorization: token,
      },
    });
  },
  postTransfer: async (token, data) => {
    console.log('create new transfer with data', data);
    return await axios({
      url: GlobalURL('transfer').url,
      method: GlobalURL('transfer').method,
      headers: {
        Authorization: token,
      },
      data: data,
    });
  },
};

export default BankingManager;
