import React, {createRef, useEffect, useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Button from '../components/Button';
import InputText from '../components/InputText';
import {
  GlobalColor,
  GlobalActionType,
  GlobalStyle,
} from '../configs/GlobalValue';
import {
  createNewTransfer,
  updatePayees,
} from './../redux/actions/bankingAction';
import {
  payeesDropdownTransformer,
  TransferDataset,
} from './../functions/Utilities';
import Separator from '../components/Separator';

const NewTransaction = ({navigation, route}) => {
  const passedParameter = route.params;

  const dispatch = useDispatch();

  const banking = useSelector(state => state.banking);
  const payees = useSelector(state => state.payees);
  const transaction = useSelector(state => state.transaction);

  const [payee, setPayee] = useState();
  const [payeeLoading, setPayeeLoading] = useState(true);
  const [amount, setAmount] = useState();
  const [description, setDescription] = useState('');
  const [payeeError, setPayeeError] = useState();
  const [amountError, setAmountError] = useState();

  const amountRef = createRef();

  const handleBackButton = () => {
    navigation.goBack();
  };

  const handleTransfer = () => {
    if (payee == null) {
      setPayeeError('Transfer recipient yet to be selected');
      return;
    }
    if (amount == null || amount == 0) {
      setAmountError('Transfer amount yet to be specified');
      return;
    }
    const transferData = new TransferDataset();
    transferData.receipientAccountNo = payee;
    transferData.amount = parseInt(amount);
    transferData.description = description;

    dispatch(createNewTransfer(transferData.toJson()));
  };

  useEffect(() => {
    if (transaction.success && !transaction.loading)
      return navigation.navigate('TransactionSummary', {
        transaction,
        fromNewTransaction: true,
      });
  }, [transaction]);

  useEffect(() => {
    setPayeeLoading(payees.loading);
  }, [payees]);

  useEffect(() => {
    dispatch(updatePayees());
    return () => {
      dispatch({type: GlobalActionType.transferReset});
    };
  }, []);

  return (
    <View style={[GlobalStyle.ScreenBase, {padding: 15}]}>
      <Text style={GlobalStyle.TextScreenHeader}>New Transaction</Text>
      <View style={{flex: 1}}>
        <InputText
          label="Recipient"
          dropdown
          dropdownLoading={payeeLoading}
          dropdownData={payeesDropdownTransformer(payees.payeesList)}
          onDropdownChange={v => setPayee(v)}
          dropdownIndex={20}
          onSubmitEditing={() => amountRef.current.focus()}
          error={payeeError}
          onFocus={() => setPayeeError()}
        />
        {payeeError && (
          <Text style={{color: GlobalColor.error}}>{payeeError}</Text>
        )}
        <InputText
          label="Amount"
          isCurrency
          value={amount}
          onChangeText={t => {
            setAmount(t);
            if (banking.userBalance < parseInt(t)) {
              setAmountError('Transfer amount exceed your current balance');
              return;
            }
            if (amountError) {
              setAmountError();
            }
          }}
          ref={amountRef}
          onSubmitEditing={() => {
            if (banking.userBalance < parseInt(amount)) {
              setAmountError('Transfer amount exceed your current balance');
            }
          }}
          error={amountError}
          onFocus={() => setAmountError()}
        />
        {amountError && (
          <Text style={{color: GlobalColor.error}}>{amountError}</Text>
        )}
        <InputText
          label="Description (optional)"
          value={description}
          onChangeText={t => setDescription(t)}
          multiline
          numberOfLines={4}
        />
        <Text
          style={{
            textAlign: 'right',
            marginRight: 15,
            color: GlobalColor.lightest,
          }}>
          {description.length}/120
        </Text>
      </View>
      <Button label={'Transfer Now'} onPress={handleTransfer} />
      <Separator height={5} />
      <Button
        label={'Cancel'}
        mode="text"
        onPress={handleBackButton}
        textStyle={{color: GlobalColor.grey}}
      />
    </View>
  );
};

export default NewTransaction;

const styles = StyleSheet.create({});
