import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import ToastLayout from './../components/ToastLayout';

const DevScreen = ({navigation, route}) => {
  const passedParameter = route.params;
  console.log('DevScreen passed parameter:', passedParameter);

  const toastProps = {
    onPress: () => {
      console.log('pressed');
    },
    hide: () => {
      console.log('hide');
    },
    text1: 'Lets test the toast',
  };

  return (
    <View>
      <ToastLayout props={toastProps} />
      <ToastLayout props={toastProps} notice />
      <ToastLayout props={toastProps} success />
      <ToastLayout props={toastProps} warning />
    </View>
  );
};

export default DevScreen;

const styles = StyleSheet.create({});
