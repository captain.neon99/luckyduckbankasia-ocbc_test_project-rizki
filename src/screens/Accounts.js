import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Button from '../components/Button';
import CardTransaction from '../components/CardTransaction';
import Separator from '../components/Separator';
import UserBalance from '../components/UserBalance';
import UserCard from '../components/UserCard';
import {GlobalStyle} from '../configs/GlobalValue';
import {getCardColor, transactionSorter} from '../functions/Utilities';
import ColorButton from './../components/ColorButton';
import {GlobalActionType, GlobalColor} from './../configs/GlobalValue';

const Accounts = ({navigation, route}) => {
  const passedParameter = route.params;
  console.log('Accounts passed parameter:', passedParameter);
  const dispatch = useDispatch();

  const [selected, setSelected] = useState(0);

  const user = useSelector(state => state.user.userData);
  const balance = useSelector(state => state.banking.userBalance);
  const globalState = useSelector(state => state.global.userGlobalState);
  const transaction = useSelector(state => state.banking.userTransaction);

  console.log('Global State', globalState);

  const userGlobalState = globalState.find(
    item => item.userAccountNumber == user.userAccountNumber,
  );

  const colorScheme = [0, 1, 2];

  const handleColorPress = item => {
    setSelected(item);
    dispatch({
      type: GlobalActionType.globalStateCardUpdate,
      payload: {
        accountNo: user.userAccountNumber,
        cardColorScheme: item,
      },
    });
  };

  const handleBackPress = () => {
    navigation.goBack();
  };

  const handleTransactionPress = item => {
    navigation.navigate('TransactionSummary', {transactionDetails: item});
  };

  useEffect(() => {
    setSelected(userGlobalState.userCardData.cardColorScheme);
  }, []);

  return (
    <View style={[GlobalStyle.ScreenBase]}>
      <ScrollView style={{flex: 1}}>
        <View style={{padding: 15}}>
          <Text style={GlobalStyle.TextScreenHeader}>My Card</Text>
        </View>
        <UserCard
          user={user}
          balance={balance}
          cardData={
            userGlobalState?.userCardData != null
              ? userGlobalState?.userCardData
              : globalState[0].userCardData
          }
        />
        <View style={{paddingHorizontal: 15}}>
          <Text style={GlobalStyle.Content}>Card Color </Text>
          <View style={{flexDirection: 'row', marginVertical: 10}}>
            {colorScheme.map((item, index) => (
              <View key={index} style={{marginRight: 10}}>
                <ColorButton
                  color={getCardColor(item)}
                  onPress={() => handleColorPress(item)}
                  active={item == selected}
                />
              </View>
            ))}
          </View>
        </View>
        <UserBalance userBalance={balance} disable />
        <View style={{paddingHorizontal: 15, paddingBottom: 15}}>
          <Text style={GlobalStyle.TextScreenHeader}>Inbound Outbound</Text>
        </View>
        <View style={{paddingHorizontal: 15}}>
          {transactionSorter(transaction, 'inOut').map((item, index) => (
            <View
              key={index}
              style={{
                backgroundColor: GlobalColor.lightest,
                padding: 15,
                borderRadius: 5,
                elevation: 5,
                marginBottom: 10,
              }}>
              <Text style={[GlobalStyle.Balance, {color: GlobalColor.darkest}]}>
                {item.transactionType == 'transfer' ? 'Outbound' : 'Inbound'}
              </Text>
              {item.transactions.map((transactionItem, index) => (
                <CardTransaction
                  key={index}
                  cardData={transactionItem}
                  attachDate
                  onPress={() => handleTransactionPress(transactionItem)}
                />
              ))}
            </View>
          ))}
        </View>
      </ScrollView>
      <View style={{padding: 15}}>
        <Button label={'Back'} mode="bordered" onPress={handleBackPress} />
      </View>
    </View>
  );
};

export default Accounts;

const styles = StyleSheet.create({});
