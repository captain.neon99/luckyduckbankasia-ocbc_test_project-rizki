import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, BackHandler, ScrollView} from 'react-native';
import Button from '../components/Button';
import {GlobalColor, GlobalStyle} from '../configs/GlobalValue';
import {getCurrencyString, capitalizeSentence} from './../functions/Utilities';
import moment from 'moment';
import Separator from '../components/Separator';

const TransactionSummary = ({navigation, route}) => {
  const passedParameter = route.params;
  console.log('TransactionSummary passed parameter:', passedParameter);
  const transactionDetails = passedParameter.fromNewTransaction
    ? passedParameter.transaction.transactionDetails
    : passedParameter.transactionDetails;
  const isReceived =
    transactionDetails.transactionType != null &&
    transactionDetails.transactionType == 'received';
  const handleBackButton = () => {
    navigation.popToTop();
  };

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        navigation.popToTop();
        return true;
      },
    );

    return () => backHandler.remove();
  }, []);

  return (
    <View style={[GlobalStyle.ScreenBase, {padding: 15}]}>
      <Text style={GlobalStyle.TextScreenHeader}>Transaction Summary</Text>
      <Separator height={15} />
      <View
        style={{
          padding: 15,
          backgroundColor: GlobalColor.lightest,
          // margin: 15,
          borderRadius: 5,
          elevation: 5,
        }}>
        <ScrollView>
          <Text style={[GlobalStyle.Balance, {color: GlobalColor.success}]}>
            Your transaction is {transactionDetails.status ?? 'success'}
          </Text>
          <Text
            style={[
              GlobalStyle.Title,
              {color: GlobalColor.darkest, fontWeight: 'bold'},
            ]}>
            Details:
          </Text>
          <View style={{marginVertical: 15}}>
            <Text style={[GlobalStyle.Content, {color: GlobalColor.grey}]}>
              Transaction Type
            </Text>
            <Text style={[GlobalStyle.Content, {color: GlobalColor.darkest}]}>
              {isReceived ? 'Inbound Receive' : 'Outbound Transfer'}
            </Text>
            <Separator height={10} />
            {(transactionDetails.receipient || transactionDetails.sender) && (
              <>
                <Text style={[GlobalStyle.Content, {color: GlobalColor.grey}]}>
                  {isReceived ? 'Sender' : 'Recipient'} Name
                </Text>
                <Text
                  style={[GlobalStyle.Content, {color: GlobalColor.darkest}]}>
                  {transactionDetails.recipientAccount ?? isReceived
                    ? transactionDetails.sender.accountHolder
                    : transactionDetails.receipient.accountHolder}
                </Text>
                <Separator height={10} />
              </>
            )}
            <Text style={[GlobalStyle.Content, {color: GlobalColor.grey}]}>
              {isReceived ? 'Sender' : 'Recipient'} Account Number
            </Text>
            <Text style={[GlobalStyle.Content, {color: GlobalColor.darkest}]}>
              {isReceived
                ? transactionDetails.sender.accountNo
                : transactionDetails.recipientAccount ??
                  transactionDetails.receipient.accountNo}
            </Text>
            <Separator height={10} />
            <Text style={[GlobalStyle.Content, {color: GlobalColor.grey}]}>
              Transaction Time
            </Text>
            <Text style={[GlobalStyle.Content, {color: GlobalColor.darkest}]}>
              {moment(
                passedParameter.fromNewTransaction
                  ? transactionDetails.transactionLastAttemptDate
                  : transactionDetails.transactionDate,
              ).format('DD MMMM YYYY hh:mm || ')}
              {moment(
                passedParameter.fromNewTransaction
                  ? transactionDetails.transactionLastAttemptDate
                  : transactionDetails.transactionDate,
              ).fromNow()}
            </Text>
            <Separator height={10} />
            <Text style={[GlobalStyle.Content, {color: GlobalColor.grey}]}>
              Transaction ID
            </Text>
            <Text style={[GlobalStyle.Content, {color: GlobalColor.darkest}]}>
              {transactionDetails.transactionId}
            </Text>
            <Separator height={10} />

            <Text style={[GlobalStyle.Content, {color: GlobalColor.grey}]}>
              Description
            </Text>
            <Text style={[GlobalStyle.Content, {color: GlobalColor.darkest}]}>
              {capitalizeSentence(transactionDetails.description)}
            </Text>
            <Separator height={20} />
            <Text style={[GlobalStyle.Title, {color: GlobalColor.grey}]}>
              Transaction Amount
            </Text>
            <Text style={[GlobalStyle.Balance, {color: GlobalColor.primary}]}>
              {getCurrencyString(transactionDetails.amount)}
            </Text>
          </View>
        </ScrollView>
      </View>

      <Separator style={{flex: 1}} />
      <Button
        label={'Back'}
        mode="bordered"
        onPress={handleBackButton}
        // textStyle={{color: GlobalColor.grey}}
      />
    </View>
  );
};

export default TransactionSummary;

const styles = StyleSheet.create({});
