import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, FlatList, ScrollView} from 'react-native';

import {useIsFocused} from '@react-navigation/native';

import {useDispatch, useSelector} from 'react-redux';
import {
  refreshBalance,
  refreshTransaction,
} from './../redux/actions/bankingAction';

import {
  GlobalActionType,
  GlobalColor,
  GlobalStyle,
} from './../configs/GlobalValue';

import BankingManager from './../functions/BankingManager';

import Button from './../components/Button';
import Separator from '../components/Separator';
import UserCard from '../components/UserCard';
import CardTransaction from '../components/CardTransaction';
import CardBaseTransaction from './../components/CardBaseTransaction';
import UserBalance from '../components/UserBalance';
import {transactionSorter} from '../functions/Utilities';

const Dashboard = ({navigation, route}) => {
  const passedParameter = route.params;
  const dispatch = useDispatch();
  const focus = useIsFocused();

  const balance = useSelector(state => state.banking.userBalance);
  const transaction = useSelector(state => state.banking.userTransaction);

  const handleLogout = () => {
    dispatch({type: GlobalActionType.authLogout});
  };

  const handleTransactionPress = item => {
    navigation.navigate('TransactionSummary', {transactionDetails: item});
  };

  const handleNewTransaction = () => {
    navigation.navigate('NewTransaction');
  };

  const handleBalancePress = () => {
    navigation.navigate('Accounts');
  };

  useEffect(() => {
    if (!focus) return;
    dispatch(refreshBalance());
    dispatch(refreshTransaction());
  }, [focus]);

  return (
    <View style={GlobalStyle.ScreenBase}>
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 15,
          }}>
          <Text style={[GlobalStyle.TextScreenHeader]}>Home</Text>
          <Button
            label={'logout'}
            onPress={handleLogout}
            mode="text"
            textStyle={{color: GlobalColor.error}}
          />
        </View>
        <UserBalance userBalance={balance} onPress={handleBalancePress} />
      </View>
      <View style={{flex: 1}}>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={[
              GlobalStyle.TextScreenHeader,
              {marginHorizontal: 15, flex: 1},
            ]}>
            Transaction
          </Text>
          <Button
            label={'+ New Transaction'}
            mode="text"
            onPress={handleNewTransaction}
          />
        </View>
        {transaction != null && (
          <FlatList
            data={transactionSorter(transaction)}
            keyExtractor={item => item.transactionDate}
            scrollEnabled={true}
            renderItem={({item}) => {
              return (
                <CardBaseTransaction cardData={item}>
                  {item.transactions.map((transaction, index) => (
                    <CardTransaction
                      key={index}
                      cardData={transaction}
                      onPress={() => handleTransactionPress(transaction)}
                    />
                  ))}
                </CardBaseTransaction>
              );
            }}
          />
        )}
      </View>
    </View>
  );
};

export default Dashboard;

const styles = StyleSheet.create({});
