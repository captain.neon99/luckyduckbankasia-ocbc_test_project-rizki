import React from 'react';
import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import {GlobalStyle} from '../configs/GlobalValue';

const SplashScreen = ({navigation}) => {
  setTimeout(() => {
    // navigation.replace('Dev');
    navigation.replace('MainScreen');
  }, 2000);

  return (
    <View
      style={[
        GlobalStyle.ScreenBase,
        {alignContent: 'center', justifyContent: 'center'},
      ]}>
      <ImageBackground
        source={require('../assets/Taieri.png')}
        style={[
          StyleSheet.absoluteFill,
          {alignContent: 'center', justifyContent: 'center'},
        ]}>
        <Text style={[GlobalStyle.Hero, {textAlign: 'center'}]}>LDBA</Text>
        <Text style={{textAlign: 'center'}}>
          <Text style={GlobalStyle.Title}>Lucky Duck</Text>
          <Text
            style={[GlobalStyle.Title, {fontWeight: 'bold', color: 'gold'}]}>
            Bank Asia
          </Text>
        </Text>
      </ImageBackground>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({});
