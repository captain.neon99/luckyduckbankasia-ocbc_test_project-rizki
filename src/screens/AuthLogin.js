import React, {createRef, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useDispatch} from 'react-redux';
import Toast from 'react-native-toast-message';

import {GlobalColor, GlobalStyle} from './../configs/GlobalValue';

import Button from '../components/Button';
import InputText from '../components/InputText';
import Separator from './../components/Separator';
import {authLogin} from './../redux/actions/authenticationAction';

const AuthLogin = ({navigation, route}) => {
  const passedParameter = route.params;
  const dispatch = useDispatch();

  const [userName, setUserName] = useState('');
  const [userNameError, setUserNameError] = useState();
  const [userPassword, setUserPassword] = useState('');
  const [userPasswordError, setUserPasswordError] = useState();
  const [passwordSecured, setPasswordSecured] = useState(true);

  const passwordFieldRef = createRef();

  const signInHandler = () => {
    if (userName.length < 1) {
      setUserNameError('Username is required');
      return;
    }
    if (userPassword.length < 1) {
      setUserPasswordError('Password is required');
      return;
    }
    dispatch(authLogin(userName, userPassword));
  };

  const registerHandler = () => {
    navigation.navigate('Register');
  };

  return (
    <View style={[GlobalStyle.ScreenBase, {padding: 15}]}>
      <View style={{flex: 1}}>
        <Text style={GlobalStyle.TextScreenHeader}>Login</Text>
      </View>
      <View style={{flex: 3}}>
        <InputText
          label={'Username'}
          value={userName}
          onChangeText={t => setUserName(t)}
          error={userNameError}
          onSubmitEditing={() => passwordFieldRef.current.focus()}
          onFocus={() => setUserNameError()}
        />
        {userNameError && <Text style={styles.errorText}>{userNameError}</Text>}
        <InputText
          label={'Password'}
          value={userPassword}
          onChangeText={t => setUserPassword(t)}
          error={userPasswordError}
          ref={passwordFieldRef}
          secured={passwordSecured}
          onFocus={() => setUserPasswordError()}
          onSubmitEditing={signInHandler}
          suffix={passwordSecured ? 'show' : 'hide'}
          onSuffixPressed={() => setPasswordSecured(!passwordSecured)}
        />
        {userPasswordError && (
          <Text style={styles.errorText}>{userPasswordError}</Text>
        )}
      </View>
      <View style={{flex: 1, flexDirection: 'column-reverse'}}>
        <Button label={'Register'} onPress={registerHandler} mode="text" />
        <Separator height={10} />
        <Button label={'SignIn'} onPress={signInHandler} />
        {/* <Separator height={10} />
        <Button
          label={'COK'}
          onPress={() => {
            Toast.show({type: 'success', text1: 'cok'});
          }}
        /> */}
      </View>
    </View>
  );
};

export default AuthLogin;

const styles = StyleSheet.create({
  errorText: {
    color: GlobalColor.error,
  },
});
