import React, {createRef, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useDispatch} from 'react-redux';

import {GlobalColor, GlobalStyle} from './../configs/GlobalValue';

import Button from '../components/Button';
import InputText from '../components/InputText';
import Separator from './../components/Separator';
import {authRegister} from '../redux/actions/authenticationAction';
import Toast from 'react-native-toast-message';

const AuthRegister = ({navigation, route}) => {
  const passedParameter = route.params;
  const dispatch = useDispatch();

  const [userName, setUserName] = useState('');
  const [userNameError, setUserNameError] = useState();
  const [userPassword, setUserPassword] = useState('');
  const [userPasswordError, setUserPasswordError] = useState();
  const [userConfirmPassword, setUserConfirmPassword] = useState('');
  const [userConfirmPasswordError, setUserConfirmPasswordError] = useState();
  const [passwordSecured, setPasswordSecured] = useState(true);
  const [confirmPasswordSecured, setConfirmPasswordSecured] = useState(true);

  const passwordFieldRef = createRef();
  const confirmPasswordFieldRef = createRef();

  const signInHandler = () => {
    navigation.goBack();
  };

  const registerHandler = async () => {
    if (userName.length < 1) {
      setUserNameError('Username is required');
      return;
    }
    if (userPassword.length < 1) {
      setUserPasswordError('Password is required');
      return;
    }
    if (userConfirmPassword.length < 1) {
      setUserConfirmPasswordError('Confirm Password is required');
      return;
    }
    if (userPassword != userConfirmPassword) {
      setUserConfirmPasswordError(
        'Confirm Password and Password is not matched',
      );
      return;
    }
    await dispatch(authRegister(userName, userPassword)).then(res => {
      console.log('res', res);
      if (res.isSuccess) {
        Toast.show({
          type: 'success',
          text1: 'Register Success, Please login using your new credentials',
        });
        navigation.goBack();
        return;
      }
      if (res.code == 403) {
        setUserNameError(res.message.error);
      }
    });
  };

  return (
    <View style={[GlobalStyle.ScreenBase, {padding: 15}]}>
      <View style={{flex: 1}}>
        <Text style={GlobalStyle.TextScreenHeader}>Register</Text>
      </View>
      <View style={{flex: 3}}>
        <InputText
          label={'Username'}
          value={userName}
          onChangeText={t => setUserName(t)}
          error={userNameError}
          onSubmitEditing={() => passwordFieldRef.current.focus()}
          onFocus={() => setUserNameError()}
        />
        {userNameError && <Text style={styles.errorText}>{userNameError}</Text>}
        <InputText
          label={'Password'}
          value={userPassword}
          onChangeText={t => setUserPassword(t)}
          error={userPasswordError}
          ref={passwordFieldRef}
          secured={passwordSecured}
          onSubmitEditing={() => confirmPasswordFieldRef.current.focus()}
          onFocus={() => setUserPasswordError()}
          suffix={passwordSecured ? 'show' : 'hide'}
          onSuffixPressed={() => setPasswordSecured(!passwordSecured)}
        />
        {userPasswordError && (
          <Text style={styles.errorText}>{userPasswordError}</Text>
        )}
        <InputText
          label={'Confirm Password'}
          value={userConfirmPassword}
          onChangeText={t => setUserConfirmPassword(t)}
          error={userConfirmPasswordError}
          ref={confirmPasswordFieldRef}
          secured={confirmPasswordSecured}
          onFocus={() => setUserConfirmPasswordError()}
          onSubmitEditing={registerHandler}
          suffix={confirmPasswordSecured ? 'show' : 'hide'}
          onSuffixPressed={() =>
            setConfirmPasswordSecured(!confirmPasswordSecured)
          }
        />
        {userConfirmPasswordError && (
          <Text style={styles.errorText}>{userConfirmPasswordError}</Text>
        )}
      </View>
      <View style={{flex: 1, flexDirection: 'column-reverse'}}>
        <Button label={'Log In'} onPress={signInHandler} mode="text" />
        <Separator height={10} />
        <Button label={'Register'} onPress={registerHandler} />
      </View>
    </View>
  );
};

export default AuthRegister;

const styles = StyleSheet.create({
  errorText: {
    color: GlobalColor.error,
  },
});
