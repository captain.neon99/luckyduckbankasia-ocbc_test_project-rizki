export const GlobalValue = {
  baseURL: 'https://green-thumb-64168.uc.r.appspot.com/',
};

export const GlobalURL = type => {
  switch (type) {
    case 'login':
      return {url: `${GlobalValue.baseURL}login`, method: 'POST'};
    case 'register':
      return {url: `${GlobalValue.baseURL}register`, method: 'POST'};
    case 'transfer':
      return {url: `${GlobalValue.baseURL}transfer`, method: 'POST'};
    case 'balance':
      return {url: `${GlobalValue.baseURL}balance`, method: 'GET'};
    case 'payees':
      return {url: `${GlobalValue.baseURL}payees`, method: 'GET'};
    case 'transaction':
      return {url: `${GlobalValue.baseURL}transactions`, method: 'GET'};

    default:
      break;
  }
};

export const GlobalColor = {
  background: '#23265A',
  darkest: 'rgb(0, 0, 0)',
  active: 'rgb(135, 206, 235)',
  error: 'rgb(255, 45, 45)',
  // error: 'rgb(255, 99, 71)',
  primary: 'rgb(37, 103, 249)',
  secondary: '#8225F9',
  tertiary: '#FFB017',
  // primary: 'rgb(135, 206, 235)',
  lightest: '#ffffff',
  grey: 'darkgrey',
  warning: 'gold',
  success: 'rgb(21,184,37)',
  isDark: false,
};

export const GlobalStyle = {
  Hero: {
    color: GlobalColor.lightest,
    fontSize: 40,
    fontWeight: 'bold',
  },
  SubHero: {
    color: GlobalColor.lightest,
    fontSize: 10,
    // fontWeight: 'bold',
  },
  Balance: {
    color: GlobalColor.lightest,
    fontSize: 30,
    fontWeight: 'bold',
  },
  Content: {
    color: GlobalColor.lightest,
    fontSize: 16,
  },
  Title: {
    color: GlobalColor.lightest,
    fontSize: 20,
    // fontWeight: '100',
  },
  TextScreenHeader: {
    fontSize: 30,
    fontWeight: 'bold',
    color: GlobalColor.lightest,
  },
  ScreenBase: {
    backgroundColor: GlobalColor.background,
    flex: 1,
  },
};

export const GlobalActionType = {
  authInit: 'AUTH_INIT',
  authSignIn: 'AUTH_SIGN',
  authError: 'AUTH_ERROR',
  authLogout: 'AUTH_LOGOUT',
  balanceRefresh: 'BALANCE_REFRESH',
  transactionRefresh: 'TRANSACTION_REFRESH',
  payeesInit: 'PAYEES_INIT',
  payeesError: 'PAYEES_ERROR',
  payeesUpdate: 'PAYEES_UPDATE',
  transferInit: 'TRANSFER_INIT',
  transferError: 'TRANSFER_ERROR',
  transferSuccess: 'TRANSFER_SUCCESS',
  transferReset: 'TRANSFER_RESET',
  globalStateCardUpdate: 'GLOBAL_STATE_UPDATE',
};
