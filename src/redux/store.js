import {createStore, applyMiddleware, compose} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import thunk from 'redux-thunk';

// import rootReducer from './reducer/index.js';
import reducer from './reducers/index.js';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['banking', 'payees', 'transaction'],
};
const persistedReducer = persistReducer(persistConfig, reducer);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const stores = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(thunk)),
);

export let store = stores;
export let persistor = persistStore(store);
