import BankingManager from './../../functions/BankingManager';
import {GlobalActionType} from './../../configs/GlobalValue';
import Toast from 'react-native-toast-message';
import {transactionSorter} from '../../functions/Utilities';

export const refreshBalance = payload => {
  return async (dispatch, getState) => {
    const token = getState().user.userToken;

    if (token == null) return;

    return await BankingManager.getBalance(token)
      .then(res => {
        console.log('refreshBalance res', res);
        dispatch({
          type: GlobalActionType.balanceRefresh,
          payload: res.data.balance,
        });
      })
      .catch(err => {
        console.log('refreshBalance err', err.response);
        if (err.response.status == 401) {
          dispatch({
            type: GlobalActionType.authLogout,
          });
          Toast.show({
            type: 'error',
            text1: err.response.data.error.name,
          });
        } else {
          Toast.show({
            type: 'error',
            text1: err.response.data.error,
          });
        }
      });
  };
};

export const refreshTransaction = payload => {
  return async (dispatch, getState) => {
    const token = getState().user.userToken;

    if (token == null) return;

    return await BankingManager.getTransaction(token)
      .then(res => {
        console.log('refreshTransaction res', res);
        // const sortedData = transactionSorter(res.data.data);
        // sortedData;
        dispatch({
          type: GlobalActionType.transactionRefresh,
          payload: res.data.data,
        });
      })
      .catch(err => {
        console.log('refreshTransaction err', err);
      });
  };
};

export const updatePayees = payload => {
  return async (dispatch, getState) => {
    const token = getState().user.userToken;

    dispatch({type: GlobalActionType.payeesInit});

    return await BankingManager.getPayees(token)
      .then(res => {
        console.log('updatePayees res', res);
        dispatch({
          type: GlobalActionType.payeesUpdate,
          payload: res.data.data,
        });
      })
      .catch(err => {
        console.log('updatePayees err', err.response);
        dispatch({
          type: GlobalActionType.payeesUpdate,
          payload: {
            code: err.response.status,
            message: 'error fetching payees',
          },
        });
        Toast.show({
          type: 'error',
          text1: 'error fetching payees',
        });
      });
  };
};

export const createNewTransfer = payload => {
  return async (dispatch, getState) => {
    const token = getState().user.userToken;
    console.log('createNewTransfer dataset:', payload);

    dispatch({type: GlobalActionType.transferInit});

    return await BankingManager.postTransfer(token, payload)
      .then(res => {
        console.log('createNewTransfer res', res);
        dispatch({
          type: GlobalActionType.transferSuccess,
          payload: res.data,
        });
      })
      .catch(err => {
        console.log('createNewTransfer err', err.response);
      });
  };
};
