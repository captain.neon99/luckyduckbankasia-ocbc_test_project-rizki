import {GlobalActionType} from '../../configs/GlobalValue';
import AuthenticationManager from './../../functions/AuthenticationManager';
import Toast from 'react-native-toast-message';

export const authLogin = (username, password) => {
  return async (dispatch, getState) => {
    //initializing authentication process
    dispatch({type: GlobalActionType.authInit});

    await AuthenticationManager.authLogin(username, password)
      .then(res => {
        console.log('res', res);
        dispatch({type: GlobalActionType.authSignIn, payload: res.data});
        Toast.show({type: 'success', text1: 'Sign in success'});
      })
      .catch(err => {
        console.log('err', err.response.data);
        console.log('err code', err.response);
        dispatch({
          type: GlobalActionType.authError,
          payload: {...err.response.data, code: err.response.status},
        });
      });
  };
};

export const authRegister = (username, password) => {
  return async (dispatch, getState) => {
    //initializing authentication process
    // dispatch({type: GlobalActionType.authInit});

    return await AuthenticationManager.authRegister(username, password)
      .then(res => {
        console.log('register res:', res);
        return {isSuccess: true};
      })
      .catch(err => {
        console.log('register err:', err.response);
        return {
          isSuccess: false,
          message: err.response.data,
          code: err.response.status,
        };
      });
  };
};