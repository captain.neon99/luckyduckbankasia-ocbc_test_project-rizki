import {GlobalActionType} from './../../configs/GlobalValue';

const initialState = {
  loading: false,
  success: false,
  payeesList: [],
  payeesLastUpdate: null,
  code: 0,
  message: null,
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case GlobalActionType.payeesInit:
      return {...state, loading: true};

    case GlobalActionType.payeesUpdate:
      return {
        ...state,
        loading: false,
        success: true,
        code: 200,
        message: 'payees fetched',
        payeesLastUpdate: Date.now(),
        payeesList: payload,
      };

    case GlobalActionType.payeesError:
      return {
        ...state,
        loading: false,
        success: false,
        code: payload.code,
        message: payload.message,
      };

    case GlobalActionType.authLogout:
      return {
        ...state,
        loading: false,
        success: false,
        payeesList: [],
        payeesLastUpdate: null,
        code: 0,
        message: null,
      };

    default:
      return state;
  }
};
