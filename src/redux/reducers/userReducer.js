import {GlobalActionType} from '../../configs/GlobalValue';

const initialState = {
  loading: false,
  success: false,
  stage: 0,
  message: null,
  userLoginTime: null,
  userLatestChangeTime: null,
  userToken: null,
  userData: {
    userAccountNumber: null,
    userName: null,
  },
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case GlobalActionType.authInit:
      return {...state, loading: true, stage: 1};
    case GlobalActionType.authSignIn:
      return {
        ...state,
        loading: false,
        stage: 2,
        success: true,
        userLoginTime: Date.now(),
        userLatestChangeTime: Date.now(),
        userToken: payload.token,
        userData: {
          userAccountNumber: payload.accountNo,
          userName: payload.username,
        },
        message: 'Login Success',
        code: 200,
      };

    case GlobalActionType.authError:
      return {
        ...state,
        loading: false,
        success: false,
        stage: 0,
        userLatestChangeTime: Date.now(),
        userToken: null,
        userData: {
          userAccountNumber: null,
          userName: null,
        },
        message: payload.error,
        code: payload.code,
      };

    case GlobalActionType.authLogout:
      return {
        ...state,
        loading: false,
        stage: 0,
        success: false,
        userLoginTime: null,
        userLatestChangeTime: Date.now(),
        userToken: null,
        userData: {
          userAccountNumber: null,
          userName: null,
        },
        message: null,
        code: null,
      };

    default:
      return state;
  }
};
