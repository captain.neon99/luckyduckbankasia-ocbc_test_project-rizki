import {GlobalActionType} from './../../configs/GlobalValue';

const initialState = {
  userBalance: 0,
  userBalanceUpdatedDate: null,
  userTransaction: [],
  userTransactionUpdateDate: null,
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case GlobalActionType.balanceRefresh:
      return {
        ...state,
        userBalance: payload,
        userBalanceUpdatedDate: Date.now(),
      };
    case GlobalActionType.transactionRefresh:
      return {
        ...state,
        userTransaction: payload,
        userTransactionUpdateDate: Date.now(),
      };
    case GlobalActionType.authLogout:
      return {
        ...state,
        userBalance: 0,
        userBalanceUpdatedDate: null,
        userTransaction: [],
        userTransactionUpdateDate: null,
      };
    default:
      return state;
  }
};
