import {GlobalActionType} from './../../configs/GlobalValue';
const initialState = {
  userGlobalState: [
    {
      userAccountNumber: '0',
      userCardData: {
        cardColorScheme: 0,
      },
      stateLastChange: null,
    },
  ],
};

export default (state = initialState, {type, payload}) => {
  if (payload == null || payload.accountNo == null) return state;
  const currentState = {...state};
  //   console.log('payload', payload);
  const userLastRecord = currentState.userGlobalState.findIndex(
    item => item.userAccountNumber == payload.accountNo,
  );
  if (type == GlobalActionType.authSignIn && userLastRecord >= 0) {
    return state;
  }
  const newUserGlobalState = [
    ...currentState.userGlobalState,
    {
      userAccountNumber: payload.accountNo,
      userCardData: {
        cardColorScheme: 0,
      },
      stateLastChange: Date.now(),
    },
  ];
  if (type == GlobalActionType.globalStateCardUpdate) {
    currentState.userGlobalState[userLastRecord].userCardData.cardColorScheme =
      payload.cardColorScheme;
  }
  switch (type) {
    case GlobalActionType.authSignIn:
      return {
        ...state,
        userGlobalState: newUserGlobalState,
      };

    case GlobalActionType.globalStateCardUpdate:
      return {...state, ...currentState};

    default:
      return state;
  }
};
