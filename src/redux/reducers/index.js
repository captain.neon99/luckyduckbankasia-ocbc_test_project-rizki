import {combineReducers} from 'redux';
import userReducer from './userReducer';
import bankingReducer from './bankingReducer';
import payeesReducer from './payeesReducer';
import transactionReducer from './transactionReducer';
import globalReducer from './globalReducer';

export default combineReducers({
  user: userReducer,
  banking: bankingReducer,
  payees: payeesReducer,
  transaction: transactionReducer,
  global:globalReducer
});
