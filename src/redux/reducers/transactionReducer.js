import {GlobalActionType} from './../../configs/GlobalValue';

const initialState = {
  loading: false,
  success: false,
  transactionLastAttemptDate: null,
  code: null,
  message: null,
  transactionDetails: null,
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case GlobalActionType.transferInit:
      return {
        ...state,
        loading: true,
        success: false,
        transactionLastAttemptDate: Date.now(),
      };
    case GlobalActionType.transferSuccess:
      return {
        ...state,
        loading: false,
        success: true,
        code: 200,
        message: 'Transaction has been succeed',
        transactionDetails: payload,
      };
    case GlobalActionType.transferError:
      return {
        ...state,
        loading: false,
        success: false,
        code: payload.code,
        message: payload.message,
        transactionDetails: null,
      };

    case GlobalActionType.transferReset:
      return {
        ...state,
        loading: false,
        success: false,
        transactionLastAttemptDate: null,
        code: null,
        message: null,
        transactionDetails: null,
      };

    default:
      return state;
  }
};
