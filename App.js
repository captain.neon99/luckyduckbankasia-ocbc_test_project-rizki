import 'react-native-reanimated';
import 'react-native-gesture-handler';

import React, {useEffect, useState} from 'react';
import {StyleSheet, useColorScheme} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {Provider, useSelector} from 'react-redux';
import {persistor, store} from './src/redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import Toast from 'react-native-toast-message';

import ToastLayout from './src/components/ToastLayout';

import AuthLogin from './src/screens/AuthLogin';
import AuthRegister from './src/screens/AuthRegister';
import SplashScreen from './src/screens/SplashScreen';
import Dashboard from './src/screens/Dashboard';
import NewTransaction from './src/screens/NewTransaction';
import TransactionSummary from './src/screens/TransactionSummary';
import Accounts from './src/screens/Accounts';
import DevScreen from './src/screens/DevScreen';

const Stack = createNativeStackNavigator();

const BaseApp = () => {
  const [signedIn, setSignedIn] = useState(false);

  const user = useSelector(state => state.user);

  useEffect(() => {
    console.log('user', user);
    if (user.loading) return;
    if (user.success && user.userToken != null) {
      setSignedIn(true);
    } else {
      setSignedIn(false);
    }
  }, [user]);

  /**
   * Toast Config using custom layout
   */
  const toastConfig = {
    error: props => <ToastLayout props={props} />,
    success: props => <ToastLayout props={props} success />,
    warning: props => <ToastLayout props={props} warning />,
    notice: props => <ToastLayout props={props} notice />,
  };

  const MainNavigationStack = () => {
    return (
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {!signedIn ? (
          <>
            <Stack.Screen name="Login" component={AuthLogin} />
            <Stack.Screen name="Register" component={AuthRegister} />
          </>
        ) : (
          <>
            <Stack.Screen name="Dashboard" component={Dashboard} />
            <Stack.Screen name="NewTransaction" component={NewTransaction} />
            <Stack.Screen
              name="TransactionSummary"
              component={TransactionSummary}
            />
            <Stack.Screen name="Accounts" component={Accounts} />
          </>
        )}
      </Stack.Navigator>
    );
  };

  return (
    <>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="MainScreen" component={MainNavigationStack} />
          <Stack.Screen name="Dev" component={DevScreen} />
        </Stack.Navigator>
      </NavigationContainer>
      <Toast config={toastConfig} />
    </>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <BaseApp />
      </PersistGate>
    </Provider>
  );
};

export default App;
